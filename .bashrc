#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return
#GREEN="\[$(tput setaf 2)\]"
#RESET="\[$(tput sgr0)\]"
#YELLOW="\[$(tput setaf 6)\]"
#alias ls='ls --color=auto'
alias ls='exa -l --color=always --group-directories-first --icons'
#export PS1="[${GREEN}\u${RESET}@${YELLOW}\h${RESET} \W]\$ "
# Original PS1
#PS1='[\u@\h \W]\$ '
PATH=$PATH:~/.gem/ruby/2.1.0/bin/:/home/kobayashi/.gem/ruby/2.5.0/bin:~/Scripts/:/opt/Kiibohd\ Configurator/:~/.cargo/bin/:~/.local/bin/
export QUOTING_STYLE=literal
powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
source /usr/lib/python3.10/site-packages/powerline/bindings/bash/powerline.sh
alias rm='rm -i'
alias bye='shutdown'
alias s='startx'
alias n='/home/kobayashi/Scripts/next'
alias p='/home/kobayashi/Scripts/playpause'
alias c='clear'
alias boot='systemd-analyze'
